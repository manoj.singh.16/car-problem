const problem3 = (inventory) => {
  if (!inventory) return [];

  return inventory.sort((a, b) => {
    if (a.car_model.toUpperCase() < b.car_model.toUpperCase()) {
      return -1;
    }
  });
};

module.exports = problem3;
