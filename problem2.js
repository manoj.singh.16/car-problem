const problem2 = (inventory) => {
  if (!inventory) return {};

  return inventory[inventory.length - 1];
};

module.exports = problem2;
