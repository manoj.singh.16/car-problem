const problem6 = (inventory, opts) => {
  if (!inventory || !opts) return [];

  return inventory.reduce((accumulator, car) => {
    opts.forEach((option) => {
      if (car.car_make === option) {
        accumulator.push(car);
      }
    });

    return accumulator;
  }, []);
};

module.exports = problem6;
