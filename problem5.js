const problem4 = require("./problem4");

const problem5 = (inventory, year) => {
  if (!inventory || !year) return [];

  const years = problem4(inventory);

  return years.reduce((accumulator, newYear, index) => {
    if (newYear < year) {
      accumulator.push(inventory[index]);
    }

    return accumulator;
  }, []);
};

module.exports = problem5;
