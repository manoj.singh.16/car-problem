const problem4 = (inventory) => {
  if (!inventory) return [];

  return inventory.reduce((accumulator, car) => {
    accumulator.push(car.car_year);

    return accumulator;
  }, []);
};

module.exports = problem4;
